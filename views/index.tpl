<!DOCTYPE html>
<html lang='es'>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
     <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>Fanfeando</title>
    <link href='http://fonts.googleapis.com/css?family=Gudea' rel='stylesheet' type='text/css'>
    <link href="static/foundation/css/normalize.css" rel="stylesheet">
    <link href="static/fanfeando.css" rel="stylesheet">
    <link href="static/jcarousel.basic.css" rel="stylesheet">
    <link href="static/foundation/css/foundation.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
    <script type="text/javascript" src="static/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="static/js/carousel.js"></script>
</head>
<body>
    <div class='jumbotron'>
        <div class='container present'>
            <div class="row">
                <div class='large-centered columns'>
                    <img src="static/images/fanfeando.jpg" >
                </div>
            </div>
        </div>
    </div>
    <div id='manifiesto'>
        <div class='row'>
           <div class='large-8 large-centered columns'>
               <h2>Despedida</h2> 
            </div>
            <div class='large-8 large-centered columns'>
                <p style="text-align: justify;">
                    El 23 de abril del 2014 Fanfeando cerró sus operaciones.
                    Por supuesto no fue una decisión sencilla puesto que se 
                    trabajo arduamente para poder levantar el proyecto.
                    <br><br>
                    Queremos agradecer a todos los que confiarón en el proyecto y sobre todo a quienes compartierón el entusiasmo por lo que hacemos.
                    <br><br>
                    Fanfeando fue un sitio donde podias subir tus eventos,
                    ubicarlos en un mapa y lanzar invitaciones.<br>
                    Tus eventos estaban de la mejor manera para que los puedas publicar, compartir y la información relacionada con el evento sea más precisa.
                    <br><br>
                    Fanfeando fue hecho por gente que queria compartir sus eventos de manera que sea más fácil para los invitados poder buscar algún evento.
                </p>
                <p>
                    Con cariño <a href="http://twitter.com/tonsquemisa" target="_blank">Misa</a> y Yair.
                </p>
            </div>
        </div>
    </div>
    <div id='contact'>
        <div class='row'>
            <div class='row'>
                <div class='large-8 large-centered columns'>
                    <h2>Imágenes</h2>
                    <p>El proyecto murio y se rescataron algunas imagenes
                        para ayudar a visualizar como era el proyecto. </p><br>

                    <div class="jcarousel">
                        <ul>
                            <li><img src="static/images/1.png" width="650" height="450"/> </li>
                            <li><img src="static/images/2.png" width="650" height="450"/> </li>
                            <li><img src="static/images/3.png" width="650" height="450"/> </li>
                            <li><img src="static/images/4.png" width="650" height="450"/> </li>
                            <li><img src="static/images/5.png" width="650" height="450"/> </li>
                            <li><img src="static/images/6.png" width="650" height="450"/> </li>
                            <li><img src="static/images/7.png" width="650" height="450"/> </li>
                            <li><img src="static/images/8.png" width="650" height="450"/> </li>
                            <li><img src="static/images/9.png" width="650" height="450"/> </li>
                            <li><img src="static/images/10.png" width="650" height="450"/> </li>
                            <li><img src="static/images/11.png" width="650" height="450"/> </li>
                            <li><img src="static/images/12.png" width="650" height="450"/> </li>
                        </ul>
                    </div>
                    <a href="#" class="jcarousel-control-prev inactive" data-jcarouselcontrol="true">‹</a>
                    <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div id='footer'>
            <div class="copyright">
                © {{year}}. Fanfeando. Hecho con <font color="#FA5858" size="3px">♥</font>
                . Todos los derechos reservados.
                <div style="float:right;">
                        <a href="mailto:hola@fanfeando.com">
                            <img src="static/images/mail.png" width="25" style="opacity:1:">
                        </a>
                        <a href="http://twitter.com/fanfeando">
                            <img src="static//images/twitter.png" width="25" style="opacity:1;">
                        </a>
                        <a href="http://facebook.com/fanfeando">
                            <img src="static//images/facebook.png" width="25" style="opacity:1;">
                        </a>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
